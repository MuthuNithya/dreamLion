describe('workmanagement.create',function() {

    var scope, httpBackend, controller, state, createServ, data,uiGridConstants,grid,gridApi;
    beforeEach(function () {
        angular.mock.module('workmanagement.create');
    });

    beforeEach(angular.mock.module('ui.router', 'workmanagement','ui.grid','ui.grid.edit','ui.grid.cellNav','ui.grid.selection','ui.grid.autoResize'));

    describe('createController', function () {
        beforeEach(angular.mock.inject(function ($rootScope, $httpBackend, uiGridConstants,_Grid_, _GridApi_,$controller, _$q_, _$state_, $injector, historyService, WorkManagementService, $compile) {
            scope = $rootScope.$new();
            $state = _$state_;
            wms = $injector.get('WorkManagementService');
            createServ = $injector.get('createService');
            compile = $compile;
            httpBackend = $httpBackend;
            grid=_Grid_;
            gridApi=_GridApi_;
            scope.$digest();
            scope.gridOptions = {};
            scope.gridOptions.data = [
                { col1: 'col1', col2: 'col2' }
            ];

            uiGridConstants=uiGridConstants;
            spyOn($state,'go');
            $state.go('create');
            controller = $controller('createController', {
                '$scope': scope,
                '$state': $state,
                'createService': createServ,
                'WorkManagementService': wms,
                'uiGridConstants':uiGridConstants
            });

        }));

        /*it('should call grid api',function(){
            scope.grid = gridApi.grid;
            scope.gridOptions.onRegisterApi(gridApi);
        });*/


        it('should get grid height',function(){
            controller.getTableHeight();
        });

        it('should fetch effort for creating table',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([200,
                    {"status":"success"},{}
                ])
            });
            controller.selectedDateData('04/20/2016');
            httpBackend.flush();
        });

        it('should fail fetch effort for creating table',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([200,
                    {"status":"failure"},{}
                ])
            });
            controller.selectedDateData('04/20/2016');
            httpBackend.flush();
        });

        it('system should fail fetch effort call for creating table',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([500,{}
                ])
            });
            controller.selectedDateData('04/20/2016');
            httpBackend.flush();
        });

        it('should load effort table',function(){
            controller.loadEffortTable();
            spyOn(controller,'validateGridDataEmpty');
            expect(controller.validateGridDataEmpty()).toBeFalsy()
            controller.loadEffortTable();
        });

        it('should validate grid data for empty',function(){
            data =[{'fromTime':'','_fromTime':'','toTime':'','_toTime':'','description':''},{'fromTime':'','_fromTime':'','toTime':'','_toTime':'','description':''}];
            controller.validateGridDataEmpty(data);
        });

        it('should validate grid data',function(){
            data =[{'fromTime':'','_fromTime':'','toTime':'','_toTime':'','description':''},{'fromTime':'','_fromTime':'','toTime':'','_toTime':'','description':''}];
            controller.validateEffortData(data);
        });

        it('should cancel effort',function(){
            wms.prevState='login';
            controller.cancelEfforts();
            wms.prevState ='';
            controller.isSaveMoment = true;
            controller.cancelEfforts();
            wms.prevState ='';
            controller.isSaveMoment = false;
            controller.cancelEfforts();
        });


        it('should clear the effort',function(){
            scope.cleareffort();
        });

        it('should enable table edit', function () {
            controller.enableTableEdit();
        });

        it('should load effort for given date',function(){
            controller.viewEffort('04/20/2016');
        });

        it('should update effort for given data in case of saved status',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([200,
                    {
                        "status":"success",
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"saved"}
                        ]},{}
                ])
            });
            var purpose = 'Update';
            controller.showEffort('04/20/2016',purpose);
            httpBackend.flush();
        });

        it('should update effort for given data in case of frozen status',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([200,
                    {
                        "status":"success",
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = 'Update';
            controller.showEffort('04/20/2016',purpose);
            httpBackend.flush();
        });

        it('should create effort for given data in case of saved status',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([200,
                    {
                    "status":"success",
                    "results":[
                    {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"saved"}
                ]},{}
                ])
            });
            var purpose = '';
            controller.showEffort('04/20/2016',purpose);
            httpBackend.flush();
        });

        it('should create effort for given data in case of frozen status',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([200,
                    {
                        "status":"success",
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = '';
            controller.showEffort('04/20/2016',purpose);
            httpBackend.flush();
        });

        it('should fail create effort for given data',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([200,
                    {
                        "status":"failure",
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = '';
            controller.showEffort('04/20/2016',purpose);
            httpBackend.flush();
        });
        it('system should fail create effort for given data',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([500,{}
                ])
            });
            var purpose = '';
            controller.showEffort('04/20/2016',purpose);
            httpBackend.flush();
        });

        /*it('should call history date',function(){
            controller.historySelectedDate='04/20/2016';
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/details').respond(function(){
                return([500,{}
                ])
            });
            var purpose = '';
            controller.showEffort(controller.historySelectedDate,purpose);
            httpBackend.flush();
        });*/

        it('should submit effort with save parameter',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([200,
                    {
                        "status":200,
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = 'Save';
            scope.submitEffort (purpose);
            httpBackend.flush();
        });

        it('should submit effort with save parameter fail',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([200,
                    {
                        "status":500,
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = 'Save';
            scope.submitEffort (purpose);
            httpBackend.flush();
        });

        it('system should fail submit effort with save parameter',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([500,{}
                ])
            });
            var purpose = 'Save';
            scope.submitEffort (purpose);
            httpBackend.flush();
        });

        it('should submit effort with freeze parameter',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([200,
                    {
                        "status":200,
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = 'freeze';
            scope.submitEffort (purpose);
            httpBackend.flush();
        });

        it('should submit effort with freeze parameter fail',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([200,
                    {
                        "status":500,
                        "results":[
                            {"workData":"04/20/2016","isLatest":true,"version":1.0,"status":"frozen"}
                        ]},{}
                ])
            });
            var purpose = 'freeze';
            scope.submitEffort (purpose);
            httpBackend.flush();
        });

        it('system should fail submit effort with freeze parameter',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/create').respond(function(){
                return([500,{}
                ])
            });
            var purpose = 'freeze';
            scope.submitEffort (purpose);
            httpBackend.flush();
        });

        it('should validate grid data fail',function(){
            spyOn(controller,'validateEffortData');
            controller.validateEffortData();
            expect(controller.validateEffortData).toHaveBeenCalled();
            var purpose = 'freeze';
            scope.submitEffort (purpose);
        });

        it('should  grid data be empty',function(){
            spyOn(controller,'validateGridDataEmpty');
            controller.validateGridDataEmpty();
            expect(controller.validateGridDataEmpty).toHaveBeenCalled();
            var purpose = 'freeze';
            scope.submitEffort (purpose);
        });

    });

});