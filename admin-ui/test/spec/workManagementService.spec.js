describe('Testing services', function() {

    var scope,service,localStorageService,key='uName',html="<a id='ancHomeLink' class='Selected'>Home</a>",element,compile;

    beforeEach(angular.mock.module('workmanagement'));

    beforeEach(angular.mock.inject(function (WorkManagementService,_localStorageService_,$state,$compile,$rootScope) {
        service = WorkManagementService;
        localStorageService = _localStorageService_;
        spyOn($state,'go');
        scope = $rootScope.$new();
        element = angular.element(html);
        compile = $compile;
        compile(element)(scope);

    }));
    it('call set local storage',function(){
        expect(service.setLocalStorage()).toBe(true);
    });

    it('call get local storage',function(){
        spyOn(localStorageService,'get');
        service.getLocalStorage(key);
    });
    it('call remove local storage',function(){
        spyOn(localStorageService,'remove');
        service.removeLocalStorage(key);
    });

    it('call delete cookie data',function(){
        service.deleteCookieData();
    });

    it('trigger top nav click',function(){
        service.topNavClick("#ancHomeLink");

        service.topNavClick('#ancHome');

        service.topNavClick('#ancCreateWorksheet');
        service.topNavClick('#ancAbout');

        service.topNavClick('#ancContactUs');

        service.topNavClick('#ancLogin');

    });



});