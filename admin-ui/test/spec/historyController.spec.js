describe('workmanagement.history',function(){

    var scope,httpBackend,controller,state,historyServ,data;
    beforeEach(function(){
        angular.mock.module('workmanagement.history');
    });

    beforeEach(angular.mock.module('ui.router','workmanagement'));

    describe('historyController', function () {
        beforeEach(angular.mock.inject(function ($rootScope, $httpBackend, $controller, _$q_, _$state_, $injector, historyService, WorkManagementService, $compile) {
            scope = $rootScope.$new();
            $state = _$state_;
            wms = $injector.get('WorkManagementService');
            historyServ = $injector.get('historyService');
            compile = $compile;
            httpBackend = $httpBackend;
            controller = $controller('historyController', {
                '$scope': scope,
                '$state': $state,
                'historyService': historyServ,
                'WorkManagementService': wms
            });

        }));
        it('should call process data function',function(){
            data = [{'workDate':'04/20/2016','hoursLogged':8,'status':'saved','version':'1','modifiedDate':'04/25/2016','isLatest':true}];
            controller.processHistoryDate(data);
        });

        it('should show effort',function(){
            spyOn($state,'go');
            controller.showEffort('04/20/2016');
        });

        it('should filter date function fail with no data error',function(){
            controller.filterEffort('','04/25/2016');
        });

        it('should filter date function fail with date error',function(){
            controller.filterEffort('05/20/2016','04/25/2016');
        });

        it('should filter date with no errors',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/history').respond(function(){
                return([200,
                    {"status":"success"},{}
                ])
            });
            controller.filterEffort('04/20/2016','04/25/2016');
            httpBackend.flush();
        });

        it('should filter date with errors',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/history').respond(function(){
                return([200,
                    {"status":"failed"},{}
                ])
            });
            controller.filterEffort('04/20/2016','04/25/2016');
            httpBackend.flush();
        });

    });
});