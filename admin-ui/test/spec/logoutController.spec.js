describe('workmanagement.logout', function () {
    var scope,
        state = '/',
        $state,
        $injector,
        currentState,
        rootScope,
        $scope,
        $cookies,
        wms,
        controller;
    beforeEach(function () {
        angular.mock.module('workmanagement.logout');
    });
    beforeEach(angular.mock.module('ui.router','workmanagement'));
    describe('logoutController', function () {
        beforeEach(angular.mock.inject(function ($rootScope, $controller, _$state_,$injector,WorkManagementService) {
        scope=$rootScope.$new();
        $state = _$state_;
            wms = $injector.get('WorkManagementService');
            spyOn($state,'go');
        controller = $controller('logoutController', {
            '$scope': scope,
            '$state':$state,
            'WorkManagementService':wms
        });
        }));

        it('load Home page',function (){
            controller.loginagain();
        });
    });

});