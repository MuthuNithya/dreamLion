describe('workmanagement.login', function () {
    var scope,
        state = '/',
        $state,
        $injector,
        currentState,
        signUpElement="<ng-form name='loginCtrl.formSignUp' $submitted $valid></ng-form>",
        element,
        signInElement="<ng-form name='loginCtrl.formSignIn' $submitted $valid novalidate></ng-form>",
        compile,
        rootScope,
        $scope,
        $cookies,
        templateHtml,
        loginServ,
        wms,
        httpBackend,
        deferred,
        controller;
    beforeEach(function () {
        angular.mock.module('workmanagement.login');
    });
    beforeEach(angular.mock.module('ui.router','workmanagement'));

    describe('loginController', function () {
        beforeEach(angular.mock.inject(function ($rootScope,$httpBackend, $controller,_$q_, _$state_,$injector,loginService,WorkManagementService,$compile) {
            scope=$rootScope.$new();
            $state = _$state_;
            wms = $injector.get('WorkManagementService');
            loginServ = $injector.get('loginService');
            compile = $compile;
            element=angular.element(signUpElement);
            compile(element)(scope);
            scope.$digest();
            httpBackend = $httpBackend;
            controller = $controller('loginController', {
                '$scope': scope,
                '$state':$state,
                'loginService':loginServ,
                'WorkManagementService':wms
            });
        }));
        it('should call delete cookie data', function () {
            wms.deleteCookieData();
        });

        it('should call success state of sign up form',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/signup').respond(function() {
                return [200,

                    {"status":200,"message":"Signup successful"}
                    ,{}
                ]
            });
            controller.formSignUp = {
                $valid:true
            };
            controller.signUpData={
                "username":"",
                "emailId":"",
                "password":"",
                "confirmPassword":""
            };
            controller.submitForm('signup');
            httpBackend.flush();
        });


        it('should call failure state of sign up form',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/signup').respond(function() {
                return [200,
                    {"status":401}
                    ,{}
                ]
            });
            controller.formSignUp = {
                $valid:true
            };
            controller.signUpData={
                "username":"",
                "emailId":"",
                "password":"",
                "confirmPassword":""
            };
            controller.submitForm('signup');
            httpBackend.flush();
        });

        it('should sign up call get dropped',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/signup').respond(function() {
                return [500
                    ,{}
                ]
            });
            controller.formSignUp = {
                $valid:true
            };
            controller.signUpData={
                "username":"",
                "emailId":"",
                "password":"",
                "confirmPassword":""
            };
            controller.submitForm('signup');
            httpBackend.flush();
        });

        it('should call success state of sign in form',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/login').respond(function() {
                return [200,

                    {"user":{"status":"success","username":"Muthu","userid":200},"token":1234}
                    ,{}
                ]
            });
            controller.formSignIn = {
                $valid:true
            };
            controller.signInData={
                "emailId":"",
                "password":""
            };
            spyOn($state,'go');
            controller.submitForm('signin');
            httpBackend.flush();
        });


        it('should call failure state of sign in form',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/login').respond(function() {
                return [200,

                    {"status":"failure","token":1234}
                    ,{}
                ]
            });
            controller.formSignIn = {
                $valid:true
            };
            controller.signInData={
                "emailId":"",
                "password":""
            };
            controller.submitForm('signin');
            httpBackend.flush();
        });

        it('should sign up call get dropped',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/login').respond(function() {
                return [500
                    ,{}
                ]
            });
            controller.formSignIn = {
                $valid:true
            };
            controller.signInData={
                "emailId":"",
                "password":""
            };
            controller.submitForm('signin');
            httpBackend.flush();
        });


    });

});