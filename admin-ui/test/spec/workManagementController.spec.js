describe('workmanagement', function () {
    var scope,
        state='/',
        $state,
        $injector,
        currentState,
        rootScope,
        $scope,
        $cookies,
        controller;
    beforeEach(function () {
        angular.mock.module('workmanagement');
    });
    beforeEach(angular.mock.module('ui.router'));

    describe('WorkManagementController', function () {
        beforeEach(angular.mock.inject(function ($rootScope, $controller,_$state_,_$injector_,_$cookies_) {
            scope = $rootScope.$new();
            rootScope = $rootScope;
            $state = _$state_;
            $injector = _$injector_;
            $cookies = _$cookies_;
            spyOn($state, 'go');
            spyOn(rootScope, '$broadcast').and.callThrough();
            spyOn(rootScope, '$watch').and.callThrough();

            controller = $controller('WorkManagementController', {
                '$scope': scope,
                '$state':$state
            });
        }));
        it('sets the current user', function () {
            controller.currentUser = {
                "userName":"Muthu",
                "userID":"XXXX",
                "loginStatus":false
            };
        });
        it('call logout function',function(){
            controller.logout();
            expect($state.go).toHaveBeenCalledWith('logout');
        });
        it('trigger top nav',function(){
            controller.topNavClick('#ancHome');
            expect($state.go).toHaveBeenCalledWith('dashboard');
        });
        it('close modal dialog',function(){
            controller.warning = true;
            controller.closeModals();
            controller.warning = false;
            controller.timedout = true;
            controller.closeModals();
            controller.warning = false;
            controller.timedout = false;
            controller.closeModals();
        });
        it('Trigger Idle start',function(){
            rootScope.$broadcast('IdleStart');
        });
        it('Trigger Idle time out',function(){
            rootScope.$broadcast('IdleTimeout',[$state]);
            expect($state.go).toHaveBeenCalledWith('logout',({reason:'timeout'}));
        });
        it('start timer', function(){
            controller.start();
        });

        it('call stay active', function(){
            controller.stayActive();
        });

        it('call stop timer',function(){
            controller.stop();
        });

        it('watch cookie date',function(){
            spyOn($cookies,'get');
            scope.$digest();
            expect($cookies.get).toHaveBeenCalledWith('uName');
            scope.$digest();
        });

    });

});