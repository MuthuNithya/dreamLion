describe('workmanagement.dashboard',function(){

    var $state,scope,httpBackend,controller,wms,dashboardServ,compile,timeout,timerCallback;
    beforeEach(angular.mock.module('workmanagement.dashboard'));

    beforeEach(angular.mock.module('ui.router','workmanagement'));

    describe('dashboardController', function () {
        beforeEach(angular.mock.inject(function ($rootScope, $httpBackend,$timeout, $controller, _$q_, _$state_, $injector, dashboardService, WorkManagementService, $compile) {
            scope = $rootScope.$new();
            $state = _$state_;
            wms = $injector.get('WorkManagementService');
            dashboardServ = $injector.get('dashboardService');
            compile = $compile;
            httpBackend = $httpBackend;
            timeout = $timeout;
            timerCallback = jasmine.createSpy('timerCallback');
            controller = $controller('dashboardController', {
                '$scope': scope,
                '$state': $state,
                'dashboardService': dashboardServ,
                'WorkManagementService': wms,
                '$timeout':timeout
            });

        }));
        it('should show effort',function(){
            spyOn($state,'go');
            controller.showEffort('04/20/2016');

        });

        it('should call process data function',function(){
            data = [{'workDate':'04/20/2016','hoursLogged':8,'status':'saved','version':'1','modifiedDate':'04/25/2016','isLatest':true}];
            controller.processHistoryDate(data);
        });

        it('should extract chart date',function(){
            data = [{'workDate':'04/20/2016','hoursLogged':8,'status':'saved','version':'1','modifiedDate':'04/25/2016','isLatest':true},{'workDate':'04/20/2016','hoursLogged':8,'status':'saved','version':'1','modifiedDate':'04/25/2016','isLatest':true}];
            controller.extractChartDate(data);
        });

        it('should extract chart hours',function(){
            data = [{'workDate':'04/20/2016','hoursLogged':8,'status':'saved','version':'1','modifiedDate':'04/25/2016','isLatest':true},{'workDate':'04/20/2016','hoursLogged':8,'status':'saved','version':'1','modifiedDate':'04/25/2016','isLatest':true}];
            controller.extractChartHours(data);
        });

        it('should fetch activity succesfully',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/summary').respond(function(){
                return([200,
                    {"status":"success"},{}
                ])
            });
            dashboardServ.fetchActivity();
            httpBackend.flush();

        });

        it('should fail fetch activity',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/summary').respond(function(){
                return([200,
                    {"status":"failure"},{}
                ])
            });
            dashboardServ.fetchActivity();
            httpBackend.flush();

        });

        it('should system throw 500 for fetch activity',function(){
            httpBackend.whenPOST('https://heroku-node-server.herokuapp.com/api/v1/worksheets/summary').respond(function(){
                return([500,{}
                ])
            });
            dashboardServ.fetchActivity();
            httpBackend.flush();

        });

        it('should trigger top nav call with timeout',function(){
            dashboardServ.triggerTopNav();
        });
    });

});