describe('rnStepper directive', function() {
    var compile, scope, directiveElem,element;
    var html = '<input compare-to="password" ng-model="password_confirmation"/>';

    beforeEach(function () {
        angular.mock.module('workmanagement');
    });
    beforeEach(angular.mock.inject(function($rootScope, $compile) {
            compile = $compile;
            scope = $rootScope.$new();
        // load the values to send
        scope.password = 'abc';
        scope.password_confirmation = 'abc2';

        // create the element and compile it
        element = angular.element(html);
        $compile(element)(scope);

        // get the scope of this directive
        scope = element.scope();
        scope.$digest();
        }));

    it("should not valid password_confirmation", function() {
        expect(element.hasClass('ng-invalid-compare-to')).toBeTruthy();
    });

    it("should valid password_confirmation", function() {
        scope.password_confirmation = 'abc';
        scope.$digest();
        expect(element.hasClass('ng-invalid-compare-to')).toBeFalsy();
    });

});