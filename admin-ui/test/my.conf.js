module.exports = function(config) {
    config.set({
        basePath: '../',
        frameworks: ['jasmine'],
        files: [
            'app/bower_components/angular/angular.js',
            'app/bower_components/angular-mocks/angular-mocks.js',
            'app/bower_components/angular-ui-router/release/angular-ui-router.js',
            'app/bower_components/angular-cookies/angular-cookies.js',
            "app/bower_components/jquery/dist/jquery.js",
            'app/bower_components/ng-idle/angular-idle.js',
            'app/bower_components/Chart.js/Chart.js',
            'app/bower_components/angular-chart.js/dist/angular-chart.js',
            'app/bower_components/angular-ui-grid/ui-grid.js',
            'app/bower_components/angular-local-storage/dist/angular-local-storage.js',
            'app/bower_components/foundation/js/foundation.js',
            'app/bower_components/datetimepicker/jquery.datetimepicker.js',
            'app/bower_components/foundation-datepicker/js/foundation-datepicker.js',
            'assets/script/jquery.timepicker.js',
            'app/bower_components/moment/moment.js',
            'app/appconfiguration/*.js',
            'app/common/*.js',
            'app/create/*.js',
            'app/dashboard/*.js',
            'app/history/*.js',
            'app/login/*.js',
            'app/logout/*.js',
            'app/**/*.html',
            'test/spec/*.js'
        ],
        ngHtml2JsPreprocessor: {
            // we want all templates to be loaded in the same module called 'templates'
            moduleName: 'templates'
        },
        exclude:[
            '!app/appconfiguration/ngFormSubmitDirective.js'
        ],
        autoWatch : true,
        browsers: ['Chrome'],
        reporters: ['progress', 'coverage'],
        preprocessors: {'app/**/*.html': ['ng-html2js'], 'app/appconfiguration/*.js': ['coverage'],'app/common/*.js': ['coverage'],'app/create/*.js': ['coverage'],'app/dashboard/*.js': ['coverage'],'app/history/*.js': ['coverage'],'app/login/*.js': ['coverage'],'app/logout/*.js': ['coverage'] }
    });
};