module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-html-snapshot');

  grunt.initConfig({
    htmlSnapshot: {
      all: {
        options: {
          snapshotPath: 'snapshots/',
          sitePath: 'http://briztlers.com',
          urls: ['/','/about','/login','/contactus'],
          fileNamePrefix:''
        }
      }
    }
  });

  grunt.registerTask('default', ['htmlSnapshot']);
};