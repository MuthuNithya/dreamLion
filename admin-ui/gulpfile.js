// loads various gulp modules
var gulp = require('gulp');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var exclude = require('gulp-ignore').exclude;
var es = require('event-stream'),
    inject = require('gulp-inject');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass = require('gulp-sass');

var paths = {
    scss:['./app/styles/app.scss'],
    css: ['./app/styles/app.css'],
    commoncss:['./app/bower_components/foundation/css/normalize.css','./app/bower_components/foundation/css/foundation.css','./app/bower_components/foundation-sites/dist/foundation-flex.css','./app/bower_components/datetimepicker/jquery.datetimepicker.css','./app/bower_components/angular-ui-grid/ui-grid.css','./app/bower_components/angular-chart.js/dist/angular-chart.css','./assets/style/jquery.timepicker.css'],
    js: ['./app/appconfiguration/app-modules.js','./app/appconfiguration/app.js','./app/appconfiguration/app-route.js','./app/appconfiguration/app.settings.js','./app/common/*.js','./app/login/*.js','./app/dashboard/*.js','./app/logout/*.js','./app/create/*.js','./app/history/*.js','./app/profile/*.js'],
    commonjs:['./app/bower_components/jquery/dist/jquery.js','./app/bower_components/datetimepicker/jquery.datetimepicker.js','./app/bower_components/angular/angular.js','./app/bower_components/ng-idle/angular-idle.js','./app/bower_components/angular-cookies/angular-cookies.js','./app/bower_components/angular-ui-router/release/angular-ui-router.js','./app/bower_components/foundation/js/vendor/modernizr.js','./app/bower_components/foundation/js/foundation.js','app/bower_components/Chart.js/Chart.js','./app/bower_components/angular-chart.js/dist/angular-chart.js','./app/bower_components/angular-local-storage/dist/angular-local-storage.js','./app/bower_components/foundation-datepicker/js/foundation-datepicker.js','./app/bower_components/moment/moment.js','./app/bower_components/angular-ui-grid/ui-grid.js']
};

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};

gulp.task('index', function () {
// Concatenate AND minify bower JS component
   // var bowerJSStream = gulp.src(['./app/bower_components/**/*.js','!./app/bower_components/**/*.min.js','!./app/bower_components/**/index.js'])
    //    .pipe(concat('bower.js'))
     //   .pipe(uglify())
     //   .pipe(gulp.dest('./assets/min/js'));

// Concatenate AND minify node JS component
    // var nodeJSStream = gulp.src(['./node_modules/**/*.js'])
    //   .pipe(concat('node_modules.js'))
    //   .pipe(uglify())
    //   .pipe(gulp.dest('./assets/min/js'));

// Concatenate AND minify bower css component
    //var bowerCSSStream = gulp.src(['./app/bower_components/**/*.css','!./app/bower_components/**/*.min.css'])
    //    .pipe(concat('bower.css'))
    //    .pipe(minifyCSS())
    //    .pipe(gulp.dest('./assets/min/css'));

// Concatenate AND minify node css component
    //var nodeCSSStream = gulp.src(['./node_modules/**/*.css'])
    //   .pipe(concat('node_modules.css'))
    //   .pipe(minifyCSS())
    //  .pipe(gulp.dest('./assets/min/css'));

    //Concatenate and minify app css files
    var appCSSStream = gulp.src(paths.scss)
        .pipe(sass(sassOptions).on('error', sass.logError))
       .pipe(concat('app.min.css'))
       .pipe(minifyCSS())
      .pipe(gulp.dest('./assets/min/css'));

    //Concatenate and minify common css files
    var appCommonCSSStream = gulp.src(paths.commoncss)
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(concat('common.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./assets/min/css'));

    // Concatenate AND minify app JS component
    var appJSStream = gulp.src(paths.js)
       .pipe(concat('app.min.js'))
       .pipe(uglify())
       .pipe(gulp.dest('./assets/min/js'));

    // Concatenate AND minify common JS component
    var appCommonJSStream = gulp.src(paths.commonjs)
        .pipe(concat('common.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/min/js'));

    gulp.src('./index.html')
        .pipe(inject(appCSSStream, {starttag: '<!-- inject:appcss:css -->'}, {relative: true}))
        .pipe(inject(appCommonCSSStream, {starttag: '<!-- inject:commoncss:css -->'}, {relative: true}))
        .pipe(inject(appJSStream, {starttag: '<!-- inject:appjs:js -->'}, {relative: true}))
        .pipe(inject(appCommonJSStream, {starttag: '<!-- inject:commonjs:js -->'}, {relative: true}))
        .pipe(gulp.dest('./'));

});

// Static Server + watching scss/html files
gulp.task('browserSync', ['index'], function() {

    browserSync({
        server: "./"
    });

    gulp.watch('./app/**/*.scss', ['index']);
    gulp.watch('./app/**/*.js', ['index']);
    gulp.watch('./index.html').on('change', reload);
});

gulp.task('default', ['browserSync']);