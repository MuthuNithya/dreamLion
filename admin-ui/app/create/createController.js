(function() {
    "use strict";
    angular.module('workmanagement.create').controller('createController', ['$scope', '$cookies', 'createService','$q','uiGridConstants','WorkManagementService','$state', function ($scope, $cookies, createserv,$q,uiGridConstants,wms,$state) {
        var createCtrl = this;
        $scope.deleteRow;
        createserv.isupdate = false;
        createCtrl.selectedDate='';
        createCtrl.version;
        createCtrl.isSaveMoment = false;
        createCtrl.isEditable;
        createCtrl.existingDate=false;
        $('#ancHome').removeClass('Selected');
        $('#ancHistory').removeClass('Selected');
        $('#ancCreateWorksheet').addClass('Selected');
        createCtrl.historySelectedDate = $state.params.workDate;
        createCtrl.currentState = $state.current.name;
        $scope.gridOptions = createserv.initCreateTableGrid();
        $scope.gridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.deleteRow = gridApi.selection.getSelectedRows();
                $scope.successMessage = false;
                if($scope.deleteRow.length>0 && !$scope.isReadMode){
                    $('#divDeleteEffortbtn').removeClass('hide');
                } else{
                    $('#divDeleteEffortbtn').addClass('hide');
                }
            });
        };
        createCtrl.IsEditable = true;
        createCtrl.readOnly = false;
        $('#datepicker').datetimepicker({
            timepicker:false,
            mask:true,
            format:'m/d/Y',
            validateOnBlur:true,
            closeOnDateSelect:true,
            onChangeDateTime:function(dp,$input){
                $('#datepicker').blur();
                createCtrl.selectedDateData($input.val());
            },
            onShow:function( ct ){
                this.setOptions({
                    defaultDate:createCtrl.historySelectedDate?createCtrl.historySelectedDate:false
                });
            }
        });

        createCtrl.getTableHeight = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 30; // your header height
            return {
                height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
            };
        };

        createCtrl.selectedDateData = function(date){
            //$('#loadingModal').foundation('reveal', 'open');
            $('#dvLoading').show();
            var fetchEffort = createserv.fetchEffort(date);
            var all = $q.all([fetchEffort]);
            all.then(function (data) {
                if (data[0] && data[0].status) {
                    if (data[0].status == 'success') {
                        $scope.serviceError = false;
                        $scope.successMessage = false;
                        createCtrl.existingDate=false;
                        $scope.gridOptions.data=[];
                        $scope.isReadMode = false;
                        $('.effort-table').focus();
                    } else {
                        $scope.serviceError = false;
                        $scope.successMessage = false;
                        createCtrl.existingDate=true;
                        $('#dataAlreadyExist').focus();
                        $scope.isReadMode = false;
                    }
                }
                //$('#loadingModal').foundation('reveal', 'close');
                $('#dvLoading').hide();
            }, function (reject) {
                $scope.successMessage = false;
                $scope.errorMsg = 'System currently unavailable. Please try again later.';
                $scope.serviceError = true;
                //$('#loadingModal').foundation('reveal', 'close');
                $('#dvLoading').hide();
                $('.page-level-error').focus();
                $scope.isReadMode = false;
            });
        };
        createCtrl.loadEffortTable = function(){
            if(createCtrl.validateGridDataEmpty($scope.gridOptions.data)) {
                $scope.gridOptions.data.push(angular.copy(createserv.addEffortObj));
                $('.error-msg').addClass('hide');
                $('#btnCreateTimeSheet').removeClass('hide');
                return true;
            } else{
                $('.error-msg span').text('Please fill all column/s in effort table before adding new effort.');
                $('.error-msg').removeClass('hide');
            }
        };
        /*createCtrl.loadTimePicker = function(ele){
            createserv.loadTimePicker(ele);
        };*/
        createCtrl.validateGridDataEmpty = function(gridData){
            var gridlength = gridData.length;
            for( var i = 0; i<gridlength;i++){
                if(gridData[i].description === '' || gridData[i].description === 'Enter description here....' || gridData[i]._fromTime === '' || gridData[i]._toTime === ''){
                    return false;
                }
            }
            return true;
        };
        createCtrl.validateEffortData = function(gridData){
            var gridlength = gridData.length;
            var x1,x2,y1,y2;
            for( var i = 0; i<gridlength;i++) {
                for (var j = 0; j < gridlength; j++) {
                    if(i !== j) {
                        x1 = createCtrl.convertTime(gridData[i]._fromTime);
                        x2 = createCtrl.convertTime(gridData[i]._toTime);
                        y1 = createCtrl.convertTime(gridData[j]._fromTime);
                        y2 = createCtrl.convertTime(gridData[j]._toTime);
                        if (createCtrl.isOverlapping(x1,x2,y1,y2) || x1 >= x2 || y1 >= y2 ) {
                            return false;
                        }
                    }
                }
            }
            return true;
        };
        createCtrl.isOverlapping =function(x1,x2,y1,y2){
            return x1 < y2 && y1 < x2;
        };
        createCtrl.convertTime = function(timeval){
            timeval = timeval.replace(':','.');
            return (Number(timeval.toString().split('.')[0]) * 60) + Number(timeval.toString().split('.')[1]);
        };
        createCtrl.cancelEfforts = function(){
            if(wms.prevState && wms.prevState !=''){
                $scope.gridOptions.data='';
                createCtrl.showEffort(createCtrl.selectedDate);
                createserv.setIsEditable(false);
                $scope.gridOptions.enableRowSelection = false;
                $scope.isReadMode = true;
                $('#btnCreateTimeSheet').addClass('hide');
                createserv.isupdate = false;
                wms.prevState='';
            } else{
                if(createCtrl.isSaveMoment){
                    $state.go('dashboard');
                } else{
                    $('#divCancelModal').foundation('reveal','open');
                }
            }
        };
        createCtrl.mapFromTime=function(date,row,fromTime){
            var dateTime = date+' '+fromTime;
            row.fromTime = moment.utc(dateTime,"MM-DD-YYYY HH:mm").valueOf();
            return;
        };
        createCtrl.mapToTime=function(date,row,toTime){
            var dateTime = date+' '+toTime;
            row.toTime = moment.utc(dateTime,"MM-DD-YYYY HH:mm").valueOf();
            return;
        };
        $scope.cleareffort = function() {
            $scope.gridOptions.data.length=0;
            $('#btnCreateTimeSheet').addClass('hide');
            $('.error-msg').addClass('hide');
            $('#divDeleteEffortbtn').addClass('hide');
            $('#divCancelModal').foundation('reveal','close');
            $scope.successMessage = false;
            $scope.serviceError = false;
            createserv.isupdate = false;
            $scope.isReadMode = true;
        };
        createCtrl.enableTableEdit = function(){
            createserv.setIsEditable(true);
            $scope.gridOptions.enableRowSelection = true;
            $scope.isReadMode = false;
            $('#btnCreateTimeSheet').removeClass('hide');
            createserv.isupdate = true;
            wms.prevState = 'create';
        };
        createCtrl.viewEffort = function(date){
            wms.prevState = 'create';
            $state.go('view',{workDate:date});
        }
        createCtrl.showEffort = function(date,purpose){
            if(date!=''){
                var selectedDate = moment.utc(date,"MM-DD-YYYY").valueOf();
                console.log(selectedDate);
                //$('#loadingModal').foundation('reveal', 'open');
                $('#dvLoading').show();
                var fetchEffort = createserv.fetchData(selectedDate);
                var all = $q.all([fetchEffort]);
                all.then(function (data) {
                    if (data[0] && data[0].status) {
                        if (data[0].status == 'success') {
                            if(purpose==='Update'){
                                $scope.gridOptions.data='';
                                $scope.gridOptions.data = data[0].results[0].workData;
                                if(data[0].results[0].version){
                                    createCtrl.version = data[0].results[0].version;
                                }
                                if (data[0].results[0].status === 'Saved' || data[0].results[0].status === 'saved') {
                                    createCtrl.isEditable = true;
                                } else {
                                    createCtrl.isEditable = false;
                                    $('#btnCreateTimeSheet').hide();
                                    createserv.setIsEditable(false);
                                    $scope.isReadMode = true;
                                }
                            } else {
                                $('.error-msg').addClass('hide');
                                $scope.successMessage = false;
                                $scope.serviceError = false;
                                createCtrl.existingDate = false;
                                $scope.gridOptions.data='';
                                $scope.gridOptions.data = data[0].results[0].workData;
                                if(data[0].results[0].version){
                                    createCtrl.version = data[0].results[0].version;
                                }
                                if (data[0].results[0].status === 'Saved' || data[0].results[0].status === 'saved') {
                                    createCtrl.isEditable = true;
                                } else {
                                    createCtrl.isEditable = false;
                                    $('#btnCreateTimeSheet').hide();
                                }
                                createserv.setIsEditable(false);
                                //$scope.gridOptions.enableRowSelection = false;
                                //$scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.OPTIONS);
                                $scope.isReadMode = true;
                            }
                        } else {
                            $scope.errorMsg = data[0].err_msg || data[0].message;
                            $scope.successMessage = false;
                            $scope.serviceError = true;
                        }
                    };
                    //$('#loadingModal').foundation('reveal', 'close');
                    $('#dvLoading').hide();
                }, function (reject) {
                    console.log('Registration failed');
                    $scope.successMessage = false;
                    $scope.errorMsg = 'System currently unavailable. Please try again later.';
                    $scope.serviceError = true;
                    //$('#loadingModal').foundation('reveal', 'close');
                    $('#dvLoading').hide();
                });
            }
        };

        if(createCtrl.historySelectedDate){
            $('#ancCreateWorksheet').removeClass('Selected');
            createCtrl.selectedDate=createCtrl.historySelectedDate;
            console.log(createCtrl.selectedDate);
            createCtrl.showEffort(createCtrl.historySelectedDate);
        }
        $scope.submitEffort = function(parameter){
            if(createCtrl.validateGridDataEmpty($scope.gridOptions.data)) {
                if(createCtrl.validateEffortData($scope.gridOptions.data)) {
                    //$('#loadingModal').foundation('reveal', 'open');
                    $('#dvLoading').show();
                    if (parameter === 'Save') {
                        createCtrl.isSaveMoment = true;
                        if(createCtrl.version){
                            var createEffort = createserv.saveEffort(moment.utc(createCtrl.selectedDate,"MM-DD-YYYY").valueOf(), $scope.gridOptions.data,createCtrl.version);
                        } else {
                            var createEffort = createserv.saveEffort(moment.utc(createCtrl.selectedDate,"MM-DD-YYYY").valueOf(), $scope.gridOptions.data);
                        }
                        var all = $q.all([createEffort]);
                        all.then(function (data) {
                            if (data[0] && data[0].status) {
                                if (data[0].status == 200) {
                                    createCtrl.showEffort(createCtrl.selectedDate,'Update');
                                    $scope.positiveMsg = data[0].message;
                                    $('.error-msg').addClass('hide');
                                    $scope.successMessage = true;
                                    $scope.serviceError = false;
                                    $('#btnCreateTimeSheet').removeClass('hide');
                                    /*createserv.setIsEditable(false);
                                    $scope.isReadMode = true;
                                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);*/
                                } else {
                                    $scope.errorMsg = data[0].err_msg || data[0].message;
                                    $scope.successMessage = false;
                                    $scope.serviceError = true;
                                }
                            };
                            //$('#loadingModal').foundation('reveal', 'close');
                            $('#dvLoading').hide();
                        }, function (reject) {
                            console.log('Registration failed');
                            $scope.successMessage = false;
                            $scope.errorMsg = 'System currently unavailable. Please try again later.';
                            $scope.serviceError = true;
                            //$('#loadingModal').foundation('reveal', 'close');
                            $('#dvLoading').hide();
                        });
                    } else{
                        createCtrl.isSaveMoment = false;
                        if(createCtrl.version){
                            var createEffort = createserv.freezeEffort(moment.utc(createCtrl.selectedDate,"MM-DD-YYYY").valueOf(),$scope.gridOptions.data,createCtrl.version);
                        } else {
                            var createEffort = createserv.freezeEffort(moment.utc(createCtrl.selectedDate,"MM-DD-YYYY").valueOf(), $scope.gridOptions.data);
                        }
                        var all = $q.all([createEffort]);
                        all.then(function (data) {
                            if (data[0] && data[0].status) {
                                if (data[0].status == 200) {
                                    createCtrl.showEffort(createCtrl.selectedDate,'Update');
                                    $scope.positiveMsg = 'Data frozen successfully';
                                    $('.error-msg').addClass('hide');
                                    $scope.successMessage = true;
                                    $scope.serviceError = false;
                                    $('#btnCreateTimeSheet').removeClass('hide');
                                    /*createserv.setIsEditable(false);
                                    $scope.isReadMode = true;
                                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);*/
                                } else {
                                    $scope.errorMsg = data[0].err_msg || data[0].message;
                                    $scope.serviceError = true;
                                    $scope.successMessage = false;
                                }
                            };
                            $('#freezeConfModal').foundation('reveal', 'close');
                            $('#dvLoading').hide();
                        }, function (reject) {
                            console.log('Registration failed');
                            $scope.successMessage = false;
                            $scope.errorMsg = 'System currently unavailable. Please try again later.';
                            $scope.serviceError = true;
                            $('#freezeConfModal').foundation('reveal', 'close');
                            $('#dvLoading').hide();
                        });
                    }
                } else {
                        $scope.successMessage = false;
                        $scope.serviceError = false;
                        $('.error-msg span').text('From time & To time overlaps in records. Please review.');
                        $('.error-msg').removeClass('hide');
                        $('#freezeConfModal').foundation('reveal', 'close');
                }
            } else{
                $scope.successMessage = false;
                $scope.serviceError = false;
                $('.error-msg span').text('Please fill all column/s in effort table before adding new effort.');
                $('.error-msg').removeClass('hide');
                $('#freezeConfModal').foundation('reveal', 'close');
            }
        };
        createCtrl.deleteRows = function(){
            angular.forEach($scope.deleteRow, function (data, index) {
                $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
            });
            $scope.successMessage = false;
            $scope.serviceError = false;
            $('#divDeleteEffortbtn').addClass('hide');
        };
    }]);
})();