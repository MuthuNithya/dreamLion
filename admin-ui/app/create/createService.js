(function () {
    "use strict";
    angular.module('workmanagement.create').factory('createService',['$cookies','$state','$http','$q','config',function($cookies,$state,$http,$q,config) {
        var createserv = {};
        createserv.isEditable = true;
        createserv.isupdate = false;
        createserv.addEffortObj = {'fromTime':'','_fromTime':'','toTime':'','_toTime':'','description':''};
        createserv.setIsEditable = function(val){
            createserv.isEditable = val;
        };
        createserv.fetchEffort = function(date){
            var selectedWorkDate = {"workDate":moment.utc(date).valueOf()};
            var deferred = $q.defer();
            $http({
                method:'POST',
                //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                url:config.url +'/api/v1/worksheets/create',
                data:selectedWorkDate,
                "Content-Type": "application/json",
                headers:{
                    'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                    'wm-target': 'WM_VALIDATE_DATE'
                }
            }).success(function(data){
                deferred.resolve(data);
            }).error(function(data){
                deferred.reject(data);
            });
            return deferred.promise;
        };
        createserv.fetchData = function(date){
            var selectedWorkDate = {"workDate":date};
            var deferred = $q.defer();
            $http({
                method:'POST',
                //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/details',
                url:config.url +'/api/v1/worksheets/details',
                data:selectedWorkDate,
                "Content-Type": "application/json",
                headers:{
                    'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                    'wm-target': 'WM_FETCH'
                }
            }).success(function(data){
                deferred.resolve(data);
            }).error(function(data){
                deferred.reject(data);
            });
            return deferred.promise;
        };
        function cellEditCondition($scope){
            return !$scope.grid.appScope.isReadMode;
        }
        createserv.initCreateTableGrid = function() {
            var gridOptions;
            return gridOptions = {
                data :[],
                rowHeight: 30,
                enableSorting: false,
                enableCellEditOnFocus: true,
                enableColumnMenus:false,
                enableRowSelection: true,
                enableSelectAll: false,
                enableFiltering: false,
                enableHorizontalScrollbar:0,
                enableVerticalScrollbar:2,
                exporterSuppressColumns : [ '_fromTime','_toTime' ],
                columnDefs: [
                    {
                        field: '_fromTime',
                        displayName: 'From',
                        resizable: false,
                        width: '25%',
                        cellClass:'fromTime',
                        editableCellTemplate:'<input type="text" class="time" jquery-timepicker="" ng-model="row.entity._fromTime" ng-change="grid.appScope.createCtrl.mapFromTime(grid.appScope.createCtrl.selectedDate,row.entity,row.entity._fromTime);" placeholder="HH:MM"/>',
                        cellEditableCondition: cellEditCondition
                    },
                    {
                        field: '_toTime',
                        displayName: 'To',
                        resizable: false,
                        width: '25%',
                        cellClass:'toTime',
                        editableCellTemplate:'<input type="text" class="time" jquery-timepicker="" ng-model="row.entity._toTime" ng-change="grid.appScope.createCtrl.mapToTime(grid.appScope.createCtrl.selectedDate,row.entity,row.entity._toTime);" placeholder="HH:MM"/>',
                        cellEditableCondition: cellEditCondition
                    },
                    {
                        field: 'description',
                        displayName: 'Effort Description',
                        resizable: false,
                        enableHiding: false,
                        type: 'string',
                        width: '47%',
                        cellClass:'effortDesc',
                        editableCellTemplate:'<input type="text" class="description" placeholder="Enter description here...." ng-model="row.entity.description"/>',
                        cellEditableCondition: cellEditCondition
                    }
                ]};
        };
        createserv.saveEffort = function(workDate,workData,version){
            if(version){
                var createEffortDate = {"status":"Saved","workDate":workDate,"version":version,"workData":workData};
            } else{
                var createEffortDate = {"status":"Saved","workDate":workDate,"workData":workData};
            }
            var deferred = $q.defer();
            if(!createserv.isupdate) {
                $http({
                    method: 'POST',
                    //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                    url: config.url +'/api/v1/worksheets/create',
                    data: createEffortDate,
                    "Content-Type": "application/json",
                    headers: {
                        'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                        'wm-target': 'WM_CREATE'
                    }
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            } else{
                $http({
                    method: 'POST',
                    //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                    url: config.url +'/api/v1/worksheets/create',
                    data: createEffortDate,
                    "Content-Type": "application/json",
                    headers: {
                        'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                        'wm-target': 'WM_UPDATE'
                    }
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            }

        };
        createserv.freezeEffort = function(workDate,workData,version){
            if(version){
                var createEffortDate = {"status":"Frozen","workDate":workDate,"version":version,"workData":workData};
            } else{
                var createEffortDate = {"status":"Frozen","workDate":workDate,"workData":workData};
            }
            var deferred = $q.defer();
            if(!createserv.isupdate) {
                $http({
                    method: 'POST',
                    //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                    url: config.url +'/api/v1/worksheets/create',
                    data: createEffortDate,
                    "Content-Type": "application/json",
                    headers: {
                        'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                        'wm-target': 'WM_CREATE'
                    }
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            } else{
                $http({
                    method: 'POST',
                    //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/create',
                    url: config.url +'/api/v1/worksheets/create',
                    data: createEffortDate,
                    "Content-Type": "application/json",
                    headers: {
                        'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                        'wm-target': 'WM_UPDATE'
                    }
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            }
        };
        return createserv;
    }]);
})();
