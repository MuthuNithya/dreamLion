(function () {
    "use strict";
    angular.module('workmanagement.history').factory('historyService',['$cookies','$state','$q','$http','config',function($cookies,$state,$q,$http,config) {
        var historyserv = {};
        historyserv.pagingOptions = {
            pageSize: 20,
            currentPage: 1
        };
        historyserv.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };

        historyserv.initHistoryTableGrid = function(){
            var gridOptions;
            gridOptions= {
                data :[],
                rowHeight: 30,
                paginationPageSizes: [20, 50, 100],
                paginationPageSize: 20,
                enableHorizontalScrollbar:0,
                enableVerticalScrollbar:2,
                columnDefs: [
                    {
                        name: 'workDate',
                        displayName:'Date',
                        enableColumnMenu:false,
                        cellTemplate: '<div class="ui-grid-cell-contents "><a href="javascript:void(0);" ng-click="grid.appScope.historyCtrl.showEffort(row.entity.workDate);" ng-if="(row.entity.isLatest)">{{row.entity.workDate}}</a><span ng-if="!(row.entity.isLatest)">{{row.entity.workDate}}</span></div>'
                    },
                    { name: 'hoursLogged',enableColumnMenu:false,
                        displayName:'Total hours entered' },
                    { name: 'status',enableColumnMenu:false,
                        displayName:'Status' },
                    { name: 'version',enableColumnMenu:false,
                        displayName:'Version' },
                    { name: 'modifiedDate',enableColumnMenu:false,
                        displayName:'Last modified date'}
                ]
            }

            return gridOptions;
        };

        historyserv.filterEffort=function(fromDate,toDate){
            var data = {fromDate:fromDate,toDate:toDate}
            var deferred = $q.defer();
            $http({
                method:'POST',
                //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/history',
                url:config.url +'/api/v1/worksheets/history',
                data:data,
                "Content-Type": "application/json",
                headers:{
                    'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                    'wm-target': 'WM_AUDIT'
                }
            }).success(function(data){
                deferred.resolve(data);
            }).error(function(data){
                deferred.reject(data);
            });
            return deferred.promise;
        };

        historyserv.HistoryEffort = function(){
            var deferred = $q.defer();
            $http({
                method:'POST',
                //url:'https://heroku-node-server.herokuapp.com/api/v1/worksheets/history',
                url:config.url +'/api/v1/worksheets/history',
                "Content-Type": "application/json",
                headers:{
                    'X-ACCESS-TOKEN': $cookies.get('tokenKey'),
                    'wm-target': 'WM_AUDIT'
                }
            }).success(function(data){
                deferred.resolve(data);
            }).error(function(data){
                deferred.reject(data);
            });
            return deferred.promise;
        };
        return historyserv;
    }]);
})();