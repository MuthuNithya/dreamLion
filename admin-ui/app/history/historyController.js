(function() {
    "use strict";
    angular.module('workmanagement.history').controller('historyController', ['$scope', '$cookies', 'historyService','WorkManagementService','$q','$state', function ($scope, $cookies, historyserv,wms,$q,$state) {
        var historyCtrl = this;
        historyCtrl.filterFromDateMissingError = false;
        historyCtrl.filterToDateMissingError = false;
        historyCtrl.filterDateIncorrectError = false;
        historyCtrl.filterDateMissingErrorMsg = '';
        historyCtrl.filterDateIncorrectErrorMsg = '';
        $scope.toDateDisabled = true;
        $scope.noDataError = false;
        $scope.noFilterData = false;
        $('#ancHistory').addClass('Selected');
        $('#fromDate').datetimepicker({
            format:'m/d/Y',
            mask: true,
            /*onShow:function( ct ){
                this.setOptions({
                    maxDate:$('#toDate').val()?$('#toDate').val():false
                })
            },*/
            onSelectDate:function(dp,$input){
                $scope.toDateDisabled = false;
            },
            timepicker:false
        });
        $('#toDate').datetimepicker({
            format:'m/d/Y',
            mask: true,
            /*onShow:function( ct ){
                this.setOptions({
                    minDate:$('#fromDate').val()?$('#fromDate').val():false
                })
            },*/
            timepicker:false
        });

        $scope.gridOptions = historyserv.initHistoryTableGrid();
        historyCtrl.showEffort = function(date){
            wms.prevState = 'history';
            $state.go('view',{workDate:date});
        };
        historyCtrl.processHistoryDate = function(data){
            var processData = [];
            var modifiedData ={'workDate':'','hoursLogged':'','status':'','version':'','modifiedDate':'','isLatest':false};
            angular.forEach(data,function(value,key){
                if(value.workDate){
                    modifiedData.workDate = moment(value.workDate).utc().format('MM-DD-YYYY');
                }
                if(value.hoursLogged) {
                    modifiedData.hoursLogged =value.hoursLogged;
                }
                if(value.status) {
                    modifiedData.status = value.status;
                }
                if(value.version) {
                    modifiedData.version = value.version;
                }
                if(value.isLatest) {
                    modifiedData.isLatest = value.isLatest;
                }
                if(value.modifiedDate) {
                    modifiedData.modifiedDate = moment(value.modifiedDate).utc().format('MM-DD-YYYY HH:MM');
                }
                processData.push(modifiedData);
                modifiedData ={'workDate':'','hoursLogged':'','status':'','version':'','modifiedDate':'','isLatest':false};
            });
            return processData;
        };
        historyCtrl.filterEffort = function(fromDate,toDate){
            var fromDateFormatted = new Date(fromDate);
            var toDateFormatted = new Date(toDate);
            if(!fromDate || toDate ==='' || !toDate || fromDate === ''){
                if(!fromDate || fromDate ===''){
                    historyCtrl.filterFromDateMissingError = true;
                    historyCtrl.filterToDateMissingError = false;
                    historyCtrl.filterDateIncorrectError = false;
                    historyCtrl.filterDateMissingErrorMsg = 'Please select from date to filter by.';
                } else{
                    historyCtrl.filterFromDateMissingError = false;
                    historyCtrl.filterToDateMissingError = true;
                    historyCtrl.filterDateIncorrectError = false;
                    historyCtrl.filterDateMissingErrorMsg = 'Please select to date to filter by.';
                }
            } else if(fromDateFormatted > toDateFormatted){
                historyCtrl.filterDateIncorrectError = true;
                historyCtrl.filterFromDateMissingError = false;
                historyCtrl.filterToDateMissingError = false;
                historyCtrl.filterDateIncorrectErrorMsg = 'From Date should be before To Date.';
            } else{
                var fromDateMoments = moment(fromDate).valueOf();
                var toDateMoments = moment(toDate).valueOf();
                historyCtrl.filterDateIncorrectError = false;
                historyCtrl.filterFromDateMissingError = false;
                historyCtrl.filterToDateMissingError = false;
                //$('#loadingModal').foundation('reveal', 'open');
                $('#dvLoading').show();
                var filterData = historyserv.filterEffort(fromDateMoments,toDateMoments);
                var all = $q.all([filterData]);
                all.then(function (data) {
                    if (data[0] && data[0].status) {
                        if (data[0].status == 'success') {
                            var processedData = historyCtrl.processHistoryDate(data[0].results);
                            $scope.gridOptions.data = processedData;
                            $scope.noFilterData = false;
                        } else {
                            $scope.noFilterData = true;
                            //$scope.errorMsg = data[0].err_msg;

                        }
                    };
                    //$('#loadingModal').foundation('reveal', 'close');
                    $('#dvLoading').hide();
                    $('.effort-table').focus();
                }, function (reject) {
                    $scope.errorMsg = 'System currently unavailable. Please try again later.';
                    $scope.serviceError = true;
                    //$('#loadingModal').foundation('reveal', 'close');
                    $('#dvLoading').hide();
                });
            }
        };

        //$('#loadingModal').foundation('reveal', 'open');
        $('#dvLoading').show();
        var historyData = historyserv.HistoryEffort();
        var all = $q.all([historyData]);
         all.then(function (data) {
         if (data[0] && data[0].status) {
             if (data[0].status == 'success') {
                 var processedData = historyCtrl.processHistoryDate(data[0].results);
                 $scope.gridOptions.data = processedData;
                 $scope.noDataError = false;
             } else {
                 $scope.noDataError = true;
                 //$scope.errorMsg = data[0].err_msg;

             }
         };
         //$('#loadingModal').foundation('reveal', 'close');
             $('#dvLoading').hide();
         }, function (reject) {
             $scope.errorMsg = data[0].err_msg || 'System currently unavailable. Please try again later.';
             $scope.serviceError = true;
             //$('#loadingModal').foundation('reveal', 'close');
             $('#dvLoading').hide();
         });
    }]);
})();