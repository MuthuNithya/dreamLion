(function() {
    "use strict";
    angular.module('workmanagement.profile').controller('profileController', ['$scope', '$state','$rootScope', function ($scope, $state,$rootScope) {
        var profileCtrl = this;
        profileCtrl.prevState =$rootScope.fromState;

        profileCtrl.goToBack = function(){
            $state.go(profileCtrl.prevState);
        };

    }]);
})();

