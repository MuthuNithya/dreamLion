(function(){
    "use strict";
    angular.module('workmanagement.login').factory('loginService',['$http','$q','config',function($http,$q,config){
        var logicSrv={};
        logicSrv.validateLogin = function(loginDetails){
            var deferred = $q.defer();
            $http({
                method:'POST',
                //url:'https://heroku-node-server.herokuapp.com/login',
                url:config.url +'/login',
                data:loginDetails,
                "Content-Type": "application/json"
            }).success(function(data){
                deferred.resolve(data);
            }).error(function(data){
                    deferred.reject(data);
            });
            return deferred.promise;

        };
        logicSrv.signupUser = function(signupDet){
            var deferred = $q.defer();
            $http({
                method:'POST',
                //url:'https://heroku-node-server.herokuapp.com/signup',
                url:config.url +'/signup',
                data:signupDet,
                "Content-Type": "application/json"
            }).success(function(data){
                deferred.resolve(data);
            }).error(function(data){
                deferred.reject(data);
            });
            return deferred.promise;

        };
        return logicSrv;

    }]);
})();
