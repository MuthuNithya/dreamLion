(function(){
"use strict";
    angular.module('workmanagement.login').controller('loginController',['$scope','loginService','$q','$state','$cookies','WorkManagementService','Idle',function($scope,loginService,$q,$state,$cookies,wms,Idle){
    var loginCtrl=this;
        $scope.serviceError = false;
        $scope.errorMsg = '';
        $scope.positiveMsg='';
        $scope.successMessage = false;
        wms.deleteCookieData();
        loginCtrl.signInData={
                            "emailId":"",
                            "password":""
                            };
        loginCtrl.signUpData={
                            "username":"",
                            "emailId":"",
                            "password":"",
                            "confirmPassword":""
        };
        loginCtrl.submitForm = function(formMode){
            if(formMode === 'signup') {
                loginCtrl.formSignUp.$submitted = true;
                if (loginCtrl.formSignUp.$valid) {
                    //$('#loadingModal').foundation('reveal', 'open');
                    $('#dvLoading').show();
                    var signupData = loginService.signupUser(loginCtrl.signUpData);
                    var all = $q.all([signupData]);
                    all.then(function (data) {
                        if (data[0] && data[0].status) {
                            if (data[0].status === 200) {
                                $scope.serviceError = false;
                                console.log('Registration success');
                                loginCtrl.signUpData.username='';
                                loginCtrl.signUpData.emailId='';
                                loginCtrl.signUpData.password='';
                                loginCtrl.signUpData.confirmPassword='';
                                loginCtrl.formSignUp.$submitted = false;
                                $scope.positiveMsg = 'Signup successful. Please login to continue';
                                $scope.successMessage = true;
                            } else {
                                $scope.serviceError = true;
                                $scope.successMessage = false;
                                $scope.errorMsg = data[0].message;
                            }
                        };
                        //$('#loadingModal').foundation('reveal', 'close');
                        $('#dvLoading').hide();
                    }, function (reject) {
                        console.log('Registration failed');
                        $scope.errorMsg = 'System currently unavailable. Please try again later.';
                        $scope.serviceError = true;
                        $scope.successMessage = false;
                        //$('#loadingModal').foundation('reveal', 'close');
                        $('#dvLoading').hide();
                    });
                }
            } else {
                loginCtrl.formSignIn.$submitted = true;
                if (loginCtrl.formSignIn.$valid) {
                    var userDet = loginService.validateLogin(loginCtrl.signInData);
                    //$('#loadingModal').foundation('reveal', 'open');
                    $('#dvLoading').show();
                    var all = $q.all([userDet]);
                    all.then(function (data) {
                        if (data[0]) {
                            if (data[0].user) {
                                if (data[0].user.status === 'success') {
                                    $scope.serviceError = false;
                                    console.log('Authetication success');
                                    var expireDate = new Date();
                                    expireDate.setDate(expireDate.getDate() + 1);
                                    $cookies.put('uName', data[0].user.username, {'expires': expireDate});
                                    $cookies.put('uID', data[0].user.userid, {'expires': expireDate});
                                    $cookies.put('lStatus', true, {'expires': expireDate});
                                    $cookies.put('tokenKey', data[0].token, {'expires': expireDate});
                                    $state.go('dashboard');
                                    wms.getCookieData();
                                    Idle.watch();
                                }
                            } else if (data[0].status === 'failure') {
                                $scope.serviceError = true;
                                $scope.successMessage = false;
                                $scope.errorMsg = data[0].err_msg;
                                $cookies.remove('uName');
                                $cookies.remove('uID');
                                $cookies.remove('lStatus');
                                wms.getCookieData();
                            }
                        };
                        //$('#loadingModal').foundation('reveal', 'close');
                        $('#dvLoading').hide();
                    }, function (reject) {
                        console.log('Authetication failed');
                        $scope.errorMsg = 'System currently unavailable. Please try again later.';
                        $scope.serviceError = true;
                        $scope.successMessage = false;
                        $cookies.remove('uName');
                        $cookies.remove('uID');
                        wms.getCookieData();
                        //$('#loadingModal').foundation('reveal', 'close');
                        $('#dvLoading').hide();
                    });
                }
                console.log(loginCtrl.signUpData);
            }
        };
    }]);
})();
