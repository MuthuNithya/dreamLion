(function() {
    "use strict";
    angular.module('workmanagement.logout').controller('logoutController', ['$scope', 'WorkManagementService','logoutService','Idle','$state', function ($scope, wms,los,Idle,$state) {
        var loc = this;
        loc.timeOutState = $state.params.reason;
        wms.deleteCookieData();
        Idle.unwatch();
        loc.loginagain=function(){
            los.loadhomepage();
        };

    }]);
})();