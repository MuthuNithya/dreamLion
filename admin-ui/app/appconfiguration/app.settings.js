(function(){
    var app = angular.module('workmanagement');

    app.constant('config', {
        localUrl: 'http://192.168.1.68:8000',
        prodUrl: 'https://heroku-node-server.herokuapp.com',
        url:'https://heroku-node-server.herokuapp.com'
            });
})();
