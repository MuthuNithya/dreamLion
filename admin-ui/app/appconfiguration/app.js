/**
 * Created by nithya_r04 on 16/08/15.
 */
(function(){
    var app=angular.module('workmanagement',['workmanagement.common','ui.router','workmanagement.login','workmanagement.dashboard','workmanagement.history','ngCookies','workmanagement.logout','workmanagement.create','LocalStorageModule','ngIdle','workmanagement.profile']);
})();