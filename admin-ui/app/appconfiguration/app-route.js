(function(){
    angular.module('workmanagement',['ui.router','workmanagement.login','workmanagement.dashboard','workmanagement.logout','workmanagement.create','workmanagement.history','workmanagement.profile','LocalStorageModule','ngIdle']).config(['$stateProvider','$urlRouterProvider','localStorageServiceProvider','KeepaliveProvider', 'IdleProvider','$locationProvider', function($stateProvider,$urlRouterProvider,localStorageServiceProvider,KeepaliveProvider, IdleProvider,$locationProvider){
        /*localStorageServiceProvider
            .setPrefix('WorkManagement')
            .setStorageCookieDomain('')
            .setStorageType('sessionStorage')
            .setNotify(true, true);
        */

        IdleProvider.idle(600);
        IdleProvider.timeout(300);
        //IdleProvider.idle(10);
        //IdleProvider.timeout(5);
        KeepaliveProvider.interval(300);
        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');
            $state.go('login');
        });
        $urlRouterProvider.when('/dashboard','dashboard');
        $urlRouterProvider.when('/create','create');
        $urlRouterProvider.when('/history','history');
        $urlRouterProvider.when('/about','about');
        $urlRouterProvider.when('/contactus','contactus');
        $locationProvider.html5Mode({enabled:true,requireBase:false}).hashPrefix('!');

        $stateProvider
            .state('home',{
                url:'/',
                templateUrl:'app/login/login.html',
                controller:'loginController',
                controllerAs:'loginCtrl'
            })
            .state('login',{
                url:'/login',
                templateUrl:'app/login/login.html',
                controller:'loginController',
                controllerAs:'loginCtrl'
            })
            .state('dashboard',{
                url:'/dashboard',
                templateUrl:'app/dashboard/dashboard.html',
                controller: 'dashboardController',
                controllerAs: 'dashboardCtrl'
            })
            .state('create',{
                url:'/create',
                templateUrl:'app/create/create.html',
                controller: 'createController',
                controllerAs:'createCtrl'
            })
            .state('view',{
                url:'/view:workDate',
                templateUrl:'app/create/create.html',
                controller: 'createController',
                controllerAs:'createCtrl'
            })
            .state('edit',{
                url:'/edit:workDate',
                templateUrl:'app/create/create.html',
                controller: 'createController',
                controllerAs:'createCtrl'
            })
            .state('history',{
                url:'/history',
                templateUrl:'app/history/history.html',
                controller: 'historyController',
                controllerAs:'historyCtrl'
            })
            .state('logout',{
                url:'/logged-out:reason',
                templateUrl:'app/logout/logout.html',
                controller: 'logoutController',
                controllerAs: 'logoutCtrl'
            })
            .state('about',{
                url:'/about',
                templateUrl:'app/profile/about.html',
                controller:'profileController',
                controllerAs:'profileCtrl'
            })
            .state('contactus',{
                url:'/contactus',
                templateUrl:'app/profile/contactus.html',
                controller:'profileController',
                controllerAs:'profileCtrl'
            })
    }]);
})();