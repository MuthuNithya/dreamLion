/**
 * Created by MS453 on 8/16/2015.
 */
(function(){
    "use strict";
    angular.module('workmanagement').controller('WorkManagementController',['$scope','$cookies','WorkManagementService','Idle', 'Keepalive','$state','$rootScope',function($scope,$cookies,wms,Idle,Keepalive,$state,$rootScope){
        var wmc = this;
        wmc.currentUser = wms.getCookieData();
        wmc.logout = function(){
            wms.logout();
        };
        wmc.topNavClick= function (linkid) {
            wms.topNavClick(linkid);
        };

        wmc.started = false;

        wmc.closeModals = function() {
            if (wmc.warning) {
                $('#warningModal').foundation('reveal', 'close');
                wmc.warning = null;
            } else if (wmc.timedout) {
                wmc.timedout = null;
            } else{
                wmc.warning = false;
                wmc.timedout = false;
            }
        };

        $scope.$on('IdleStart', function() {
            wmc.closeModals();
            wmc.warning = true;
            $('#warningModal').foundation('reveal', 'open');
        });

        $scope.$on('IdleTimeout', function() {
            wmc.closeModals();
            wmc.timedout = true;
            $state.go('logout',{reason:'timeout'});
        });

        wmc.start = function() {
            wmc.closeModals();
            Idle.watch();
            wmc.started = true;
        };
        wmc.stayActive = function(){
            wmc.closeModals();
        };

        wmc.stop = function() {
            wmc.closeModals();
            Idle.unwatch();
            wmc.started = false;
        };

        $scope.$watch(function() {
            return $cookies.get('uName');
        }, function(newValue, oldValue) {
            wmc.currentUser = wms.getCookieData();
            console.log('wmc.currentUser',wmc.currentUser);
        });

        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            $rootScope.fromState = from.name;
            $rootScope.toState = to.name;
            //$state.go(to.name);
        });

        $scope.$on('$locationChangeStart', function(event, next, current){
            if(current.indexOf('logged-out')!== -1 && next.indexOf('login')=== -1){
                // Prevent the browser default action (Going back):
                event.preventDefault();
            } else if(current.indexOf('dashboard')!== -1 && next.indexOf('login')!== -1){
                event.preventDefault();
            } else if($rootScope.fromState && $rootScope.toState&& $rootScope.fromState === $rootScope.toState){
                $state.go($rootScope.fromState);
            }
        });

        $scope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl,newState,oldState){
            if(newUrl.indexOf('about')!==-1){
                $state.go('about');
            } else if(newUrl.indexOf('contactus')!==-1){
                $state.go('contactus');
            } else if(newUrl.indexOf('login')!==-1){
                $state.go('login');
            } else if(newUrl.indexOf('logout')!==-1){
                $state.go('logout');
            } else if(newUrl.indexOf('dashboard')!==-1){
                $state.go('dashboard');
            } else if(newUrl.indexOf('create')!==-1){
                $state.go('create');
            } else if(newUrl.indexOf('history')!==-1){
                $state.go('history');
            }

        });
    }]);
})();
