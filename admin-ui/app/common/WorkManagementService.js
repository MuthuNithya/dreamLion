(function () {
    "use strict";
    angular.module('workmanagement').factory('WorkManagementService',['$cookies','$state','localStorageService',function($cookies,$state,localStorageService){
        var wms={};
        wms.prevState='';
        wms.getCookieData=function(){
            var currentUser={
                "userName":"",
                "userID":"",
                "loginStatus":false
            };
            currentUser.userName = $cookies.get('uName');
            currentUser.userID = $cookies.get('uID');
            currentUser.loginStatus = $cookies.get('lStatus');

            return currentUser;
        };
        wms.setLocalStorage = function(key,val){
            return localStorageService.set(key, val);
        };
        wms.getLocalStorage = function(key){
            return localStorageService.get(key);
        };
        wms.removeLocalStorage = function(key) {
            return localStorageService.remove(key);
        };
        wms.deleteCookieData=function(){
            var currentUser={
                "userName":"",
                "userID":"",
                "loginStatus":false
            };
            $cookies.remove('uName');
            $cookies.remove('uID');
            $cookies.remove('lStatus');
            $cookies.remove('tokenKey');
            return currentUser;
        };
        wms.logout=function(){
            $state.go('logout');
        };
        wms.topNavClick=function(linkid){
            if($(linkid).hasClass('Selected')){
                return true;
            } else{
                if(linkid === '#ancHome'){
                    $state.go('dashboard');
                    $('a#ancHome').addClass('Selected');
                    $('a#ancCreateWorksheet').removeClass('Selected');
                    $('a#ancHistory').removeClass('Selected');
                } else if(linkid === '#ancCreateWorksheet'){
                    $state.go('create');
                    $('a#ancHome').removeClass('Selected');
                    $('a#ancCreateWorksheet').addClass('Selected');
                    $('a#ancHistory').removeClass('Selected');
                } else if(linkid === '#ancAbout') {
                    $state.go('about');
                    $('a#ancHome').removeClass('Selected');
                    $('a#ancCreateWorksheet').removeClass('Selected');
                    $('a#ancHistory').removeClass('Selected');
                } else if(linkid === '#ancContactUs') {
                    $state.go('contactus');
                    $('a#ancHome').removeClass('Selected');
                    $('a#ancCreateWorksheet').removeClass('Selected');
                    $('a#ancHistory').removeClass('Selected');
                } else if(linkid === '#ancLogin') {
                    $state.go('login');
                } else {
                    $state.go('history');
                    $('a#ancHome').removeClass('Selected');
                    $('a#ancCreateWorksheet').removeClass('Selected');
                    $('a#ancHistory').addClass('Selected');
                }
                return true;
            }
        };
        return wms;
    }]);
})();